let chai = require('chai');
let chaiHttp = require('chai-http');
var expect = chai.expect;
let server = require('../server');

chai.use(chaiHttp);

describe('/GET computed income tax value when taxableIncome < 18201', () => {
    it('it should return incometax = 0', (done) => {
          chai.request('http://localhost:3001')
          .get('/api/tax?taxableIncome=18200')
          .end((err, res) => {
              expect(res.body.incomeTax).to.be.eq(0);
             
              done();
          });
    });
});


describe('/GET computed income tax value when taxableIncome = 180001', () => {
    it('it should return incometax = 4546', (done) => {
          chai.request('http://localhost:3001')
          .get('/api/tax?taxableIncome=180001')
          .end((err, res) => {
              expect(res.body.incomeTax).to.be.eq(4546);
             
              done();
          });
    });
});